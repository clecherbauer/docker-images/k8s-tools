FROM registry.gitlab.com/clecherbauer/docker-images/debian:stretch

COPY .build /.build
RUN /.build/build.sh
